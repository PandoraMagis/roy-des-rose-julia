#Modelisation choice :
#   - tokens are 0=empty; 1=red; 2=White 
#   - Players got there points, token and color as personal var


struct Token    #token consistency data struct - create error when NaN
    token::Int
    Token(type::String) = new( type == "" ? 0 : ( type == "Red" ? 1 : (type == "White" ? 2 : NaN ) ))
end


mutable struct GameBoard
    board::Array
    GameBoard(boardHeight::Int, boardLength::Int) = new( [[Token("") for _ in 0:boardLength] for _ in 0:boardHeight] )
end

mutable struct Player
    NextPlayer::Player
    CurrentPlayer::Player

    points::Int = 0
    tokensLeft::Int = 10
    color::String = "White"

    Player() = ( current=new() ; new( Player(0,10,"Red") ) ) 
    nexTurn() = NextPlayer, CurrentPlayer = CurrentPlayer, NextPlayer
end

Token("")
Token("Red")
Token("White")

GameBoard(7,7)


